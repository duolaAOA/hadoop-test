# -*-coding:utf-8 -*-
import logging
import traceback
from os.path import exists
from loggers.logger import log

from hdfs import InsecureClient

from common import settings


class HdfsWrapper:
    """
    HDFS
    """
    def __init__(self):
        self.client = None

    def connect_hdfs(self):
        hdfs_url = getattr(settings, 'HDFS_URL', '127.0.0.1')
        hdfs_user = getattr(settings, 'HDFS_USER', 'root')
        self.client = InsecureClient(hdfs_url, user=hdfs_user)

    def mkdir_hdfs(self, path):
        if not exists(path):
            self.client.makedirs(path)

    def read_hdfs(self, hdfs_path):
        try:
            with self.client.read(hdfs_path) as reader:
                return reader.read()
        except:
            log.error(traceback.format_exc())
            self.connect_hdfs()
            log.error('reconnect hdfs...')

    def write_hdfs(self, hdfs_path, data, overwrite=False):
        try:
            with self.client.write(hdfs_path, overwrite=overwrite) as writer:
                writer.write(data)
            return hdfs_path
        except:
            log.error(traceback.format_exc())
            self.connect_hdfs()
            log.error("reconnect dhfs...")

    def delete_hdfs(self, hdfs_path, recursive=False):
        return self.client.delete(hdfs_path, recursive)
