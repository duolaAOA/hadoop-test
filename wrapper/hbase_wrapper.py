# -*-coding:utf-8 -*-
import time
import threading
import contextlib

import happybase

from common import settings
from loggers.logger import log


DEFAULT_HOST = settings.HBASE_HOST
DEFAULT_PORT = settings.HBASE_PORT
DEFAULT_TABLE_PREFIX = settings.HBASE_TABLE_PREFIX

Lock = threading.Lock()


class HbaseWrapper:
    """
    Hbase
    """
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            try:
                Lock.acquire()
                if not hasattr(cls, '_instance'):
                    cls._instance = super().__new__(cls)
            finally:
                Lock.release()
        return cls._instance

    def __init__(self, host=DEFAULT_HOST, port=DEFAULT_PORT,
                 table_prefix=DEFAULT_TABLE_PREFIX, timeout=None, autoconnect=True
                 ):
        self.hbase_host = host
        self.hbase_port = port
        self.timeout = timeout
        self.table_prefix = table_prefix
        self.autoconnect = autoconnect
        self.connection_pool = happybase.ConnectionPool(settings.HAPPYBASE_POOL_SIZE,
                                                        host=self.hbase_host,
                                                        port=self.hbase_port,
                                                        table_prefix=self.table_prefix,
                                                        autoconnect=self.autoconnect
                                                        )
        self.reconnect_count = 0
        log.info('init hbase: {}'.format(host))

    @contextlib.contextmanager
    def connect(self):
        """
        连接
        :return:
        """
        try:
            self.connection_pool.connection(self.timeout)
        except Exception as e:
            while self.reconnect_count < settings.HAPPYBASE_RECONNECT_TIMES:
                self.reconnect_count += 1
                log.error('connect hbase:  {} error,start reconnecting,has retried {} times'.
                          format(self.hbase_host, self.reconnect_count))
                self.connect()
                time.sleep(settings.HAPPYBASE_RECONNECT_WAIT_SECONDS)
            log.error('reconnect times have been max,please check the error')
            raise Exception(str(e))

    def create_table(self, table_name, families):
        """
        Create  table
        :param table_name:   table_name
        :param families:     families
        :return:
        """
        try:
            with self.connection_pool.connection(self.timeout) as connection:
                connection.create_table(
                    table_name,
                    families
                )
                log.info('target table: {} create successfully.'.format(table_name))
        except Exception as e:
            log.error('target table: {} create fail.'.format(table_name, str(e)))
            raise Exception(str(e))

    def desc_tables(self):
        try:
            with self.connection_pool.connection(self.timeout) as connection:
                tables = connection.tables()
                log.info('get all table successfully.')
                return tables
        except Exception as e:
            log.error('get all table fail: {}'.format(str(e)))
            raise Exception(str(e))

    def get_table(self, table_name):
        """
        Get Table
        :param table_name:
        :return: target_table
        """
        try:
            with self.connection_pool.connection(self.timeout) as connection:
                target_table = connection.table(table_name)
            log.degub('target table: {} get successfully.'.format(table_name))
        except Exception as e:
            log.error('target table: {} get fail.'.format(table_name, str(e)))
            raise Exception(str(e))
        return target_table

    def del_table(self, table_name):
        """
        delete table
        :param table_name:
        :return:
        """
        try:
            with self.connection_pool.connection(self.timeout) as connection:
                connection.delete_table(table_name)
            log.debug('delete table: {} successfully.'.format(table_name))
        except Exception as e:
            log.error('delete table: {} get fail.'.format(table_name, str(e)))
            raise Exception(str(e))

