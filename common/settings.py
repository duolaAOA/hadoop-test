# -*-coding:utf-8 -*-
import os

UTF_8 = 'utf-8'
SYSTEM_NAME = 'hdfs'
NAME = 'know'
HOST = os.getenv('HOST_IP')
MAX_BACK_FILE_NUM = 10
MAX_BACK_FILE_SIZE = 256 * 1024 * 1024



# happybase
# 连接池
HAPPYBASE_POOL_SIZE = 2
# 重连次数
HAPPYBASE_RECONNECT_TIMES = 100
# 睡眠时间
HAPPYBASE_RECONNECT_WAIT_SECONDS = 5

# Hbase
HBASE_HOST = HOST
HBASE_PORT = 9090
HBASE_TABLE_PREFIX = None


# mysql
MYSQL_USERNAME = 'root'
MYSQL_PASSWORD = ''
DB_URL = HOST
DB_NAME = 'test_db'

# HDFS
HDFS_URL = os.getenv('HDFS_IP')
HDFS_USER = 'root'
UPLOAD_FOLDER = '/liufei/upload'
ALLOW_EXTENSIONS = ['txt', 'pdf', 'csv', 'json', 'png', 'jpg', 'jpeg', 'gif', 'html']

# Redis
REDIS_HOST = 'http://' + HOST
REDIS_PORT = 6379


