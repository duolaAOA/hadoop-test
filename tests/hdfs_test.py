# -*-coding:utf-8 -*-
import time
from hdfs import InsecureClient
from common import settings


hdfs_client = InsecureClient(settings.HDFS_URL, user='root')
# 列出文件
hdfs_client.list('/')


# 新建文件夹
hdfs_client.makedirs('/a/b/c')

# 重命名 移动
# hdfs_client.rename('/hello.txt', '/a/b')

# 删除
hdfs_client.delete('/a', recursive=True)
print(hdfs_client.list('/'))

# with open('./test.txt', 'rb') as f:
#     data = f.read()
#     print(data)
#
#     hdfs_client.write('/hello.txt', data=data)
with hdfs_client.read('/hello.txt') as reader:
    data = reader.read()
    print(data)